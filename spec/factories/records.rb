# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :record do
    user "andr3d@mail.ru"
    stream_name "lighthouse_hall"
    ip "89.32.123.45"
    request_type "new_session"
    stream_type "hds"
    refferer "ltu.loghthouse.org/transmissions"
    auth_status "OK"
  end
end
