class RolesController < ApplicationController

  def index
    @roles = Role.all
  end

  def show
    @roles = Role.find(params[:id])
  end

  def new
    @roles = Role.new
  end

  def edit
    @roles = Role.find(params[:id])
  end

  def create
    @roles = Role.new(params[:roles])

    respond_to do |format|
      if @roles.save
        format.html { redirect_to @roles, notice: 'roles was successfully created.' }
        format.json { render json: @roles, status: :created, location: @roles }
      else
        format.html { render action: "new" }
        format.json { render json: @roles.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @roles = Role.find(params[:id])

    respond_to do |format|
      if @roles.update_attributes(params[:roles])
        format.html { redirect_to @roles, notice: 'roles was successfully updated---.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @roles.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @roles = Role.find(params[:id])
    @roles.destroy

    respond_to do |format|
      format.html { redirect_to roles_url }
      format.json { head :no_content }
    end
  end
end
