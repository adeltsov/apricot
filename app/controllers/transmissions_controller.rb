class TransmissionsController < ApplicationController
	before_filter :authenticate_user!, :except => :auth
    
    @@Sessions = {}
    
  def create_session_token
    @session_token = rand(100000000).to_s
    @@Sessions[@session_token] = Time.now + 60
  end

  def index
  	authorize_for :admin, :internal_video, :public_video 
  end

  def stream_internal
  	authorize_for :admin, :internal_video
    create_session_token
  end
  
  def audio
    authorize_for :admin, :audio
    create_session_token
  end
  
  def test_audio
    authorize_for :admin, :audio
    create_session_token
  end

  def test_video
    authorize_for :admin, :internal_video, :public_video 
    create_session_token
  end
  
  def auth
    token = params[:token]
    return head(:not_authorised) if !token
    expire_at = @@Sessions[token]
    return head(:not_found) if !expire_at
    if expire_at < Time.now
      @@Sessions.delete(token)
      return head(:request_timeout)
    end
    return head(:ok)
  end
  
  def authorize_for(*args)
    if !current_user.has_any_role?(*args)
  	redirect_to root_path, :alert => t('not_authorised')
  	end
  end
  
end
