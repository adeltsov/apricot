class UsersController < ApplicationController
  before_filter :authenticate_user!

  def index
    authorize! :index, @user, :message => t('not_authorised')
    @users = User.all
  end

  def show
    authorize! :show, @user, :message => t('not_authorised')
    @user = User.find(params[:id])
  end
  
  def update
    authorize! :update, @user, :message => t('not_authorised')
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user], :as => :admin)
      redirect_to users_path, :notice => t('user_updated')
    else
      redirect_to users_path, :alert => t('user_cannot_be_updated')
    end
  end
    
  def destroy
    authorize! :destroy, @user, :message => t('not_authorised')
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => t('user_deleted')
    else
      redirect_to users_path, :notice => t('cannot_delete_yourself')
    end
  end
end