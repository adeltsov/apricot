class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.string :user
      t.string :stream_name
      t.string :ip
      t.string :request_type
      t.string :stream_type
      t.string :refferer
      t.string :auth_status
      t.created_at
    end
  end
end
