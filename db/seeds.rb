# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
puts 'CREATING ROLES'
Role.create([
  { :name => 'admin' }, 
  { :name => 'center' }, 
  { :name => 'bk' }, 
  { :name => 'public_guest' }, 
  { :name => 'internal_video' }, 
  { :name => 'public_video' },
  { :name => 'audio' },
  { :name => 'murli' } 
], :without_protection => true)
puts 'SETTING UP DEFAULT USER LOGIN'
user1 = User.create! :name => 'Andrey Deltsov', :email => 'andrey.deltsov@gmail.com', :password => 'please', :password_confirmation => 'please', :city => 'St.Petersburg'
puts 'New user created: ' << user1.name
user2 = User.create! :name => 'Murmansk BK Center', :email => 'andr3d@mail.ru', :password => 'please', :password_confirmation => 'please', :city => 'Murmansk'
puts 'New user created: ' << user2.name
user3 = User.create! :name => 'Ivan Ivanov', :email => 'softkeeper@outlook.com', :password => 'please', :password_confirmation => 'please', :city => 'Kolomna'
puts 'New user created: ' << user3.name
user4 = User.create! :name => 'Artem Dergachev', :email => 'artem@dxdy.ru', :password => 'please', :password_confirmation => 'please', :city => 'Moscow'
puts 'New user created: ' << user4.name
user1.add_role :admin
user2.add_role :center
user3.add_role :internal_video
user4.add_role :admin