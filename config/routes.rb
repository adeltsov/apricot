Rails3BootstrapDeviseCancan::Application.routes.draw do

  resources :records

  match '/calendar(/:year(/:month))' => 'calendar#index', :as => :calendar, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}
  get "calendar/index"

  authenticated :user do
    root :to => 'transmissions#index'
  end
  root :to => 'home#index'
  
  devise_for :users
  resources :users
  resources :roles
    
  get "transmissions/index"
  get "transmissions/test_video"
  get "transmissions/stream_internal"
  get "transmissions/audio"
  get "transmissions/test_audio"
  get "transmissions/auth"
end